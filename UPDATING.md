# Updating

## How to publish a version for use by [version-app-codeclimate](https://gitlab.com/gitlab-org/version-app-codeclimate)

In general, we will use our own branches since this is a pull mirror from the original codeclimate repo.
This will allow us to update the local repo without trashing our changes.  We can then merge those updates into the
`gitlab/rubocop-*` branches as needed.
   
### GitLab Styles

* Branch off of the current rubocop version branch. For instance `gitlab/rubocop-0-82` for version `0.82`
* Modify `Gemfile` for new `gitlab-styles` version
* Run `bundle`
* Optional: run `make test`
   * Note: CI will run this step as well..in case you don't have docker installed locally.
* Open Merge Request 
   
### Rubocop

* Create a new branch off of the latest `gitlab/rubocop-*` and branch from that one for your change.
* Modify `Gemfile` for new `gitlab-styles` version
* Run `bundle`
* Update other items as needed.  
   * Some changes in the past that we've made can be seen [here](https://gitlab.com/gitlab-org/version-app-codeclimate-rubocop/-/compare/channel%2Frubocop-0-81...channel%2Frubocop-0-82)
   * Some hints can also be taken from the [Developers Guide](DEVELOPERS.md)
   * Possible easier paths could be to branch from `channel/rubocop-*` instead and then merge in some changes needed for CI and such from our `gitlab/*` branches.
* Optional: run `make test`
   * Note: CI will run this step as well..in case you don't have docker installed locally.
* Open Merge Request

### Creating a new image

* Once MR is merged, add the appropriate Tag by going [here](https://gitlab.com/gitlab-org/version-app-codeclimate-rubocop/-/tags).
* Tag must be in format as seen in the [.gitlab-ci.yml](.gitlab-ci.yml) under `release-version`/`rules`
* Adding that tag will then kick off a pipeline that will test and publish the new image with that tag [here](https://gitlab.com/gitlab-org/version-app-codeclimate-rubocop/container_registry/eyJuYW1lIjoiZ2l0bGFiLW9yZy92ZXJzaW9uLWFwcC1jb2RlY2xpbWF0ZS1ydWJvY29wIiwidGFnc19wYXRoIjoiL2dpdGxhYi1vcmcvdmVyc2lvbi1hcHAtY29kZWNsaW1hdGUtcnVib2NvcC9yZWdpc3RyeS9yZXBvc2l0b3J5LzEwNTI0ODUvdGFncz9mb3JtYXQ9anNvbiIsImlkIjoxMDUyNDg1fQ==).

### Using this image in [version-app-codeclimate](https://gitlab.com/gitlab-org/version-app-codeclimate)

Once you've created this new docker image, we can now handoff to the 
[updating instructions](https://gitlab.com/gitlab-org/version-app-codeclimate/-/blob/master-gitlab/UPDATING.md)
for info on how to use this image for our codeclimate invocation.