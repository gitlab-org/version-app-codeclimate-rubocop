This cop checks for tabs inside the source code.

### Example:
    # bad
    # This example uses a tab to indent bar.
    def foo
      bar
    end

    # good
    # This example uses spaces to indent bar.
    def foo
      bar
    end
