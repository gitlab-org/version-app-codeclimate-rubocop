
Use `assert_not` instead of `assert !`.

### Example:
    # bad
    assert !x

    # good
    assert_not x
