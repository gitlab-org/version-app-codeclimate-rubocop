Add a comment describing each gem in your Gemfile.

### Example:
    # bad

    gem 'foo'

    # good

    # Helpers for the foo things.
    gem 'foo'
