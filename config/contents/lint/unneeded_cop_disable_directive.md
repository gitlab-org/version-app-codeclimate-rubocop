# The Lint/UnneededCopDisableDirective cop needs to be disabled so as
# to be able to provide a (bad) example of an unneeded disable.
# rubocop:disable Lint/UnneededCopDisableDirective