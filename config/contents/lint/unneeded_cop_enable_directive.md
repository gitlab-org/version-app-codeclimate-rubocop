# The Lint/UnneededCopEnableDirective cop needs to be disabled so as
# to be able to provide a (bad) example of an unneeded enable.
# rubocop:disable Lint/UnneededCopEnableDirective