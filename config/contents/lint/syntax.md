This is not actually a cop. It does not inspect anything. It just
provides methods to repack Parser's diagnostics/errors
into RuboCop's offenses.